package com.pila;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Pila pila = new Pila();
        Scanner entrada = new Scanner(System.in);
        int opcion;

        do {
            System.out.println("Menu de la pila: ");
            System.out.println("1. Ingresar elementos a la pila");
            System.out.println("2. Eliminar elementos de la pila");
            System.out.println("3. Mostrar los elementos de la pila");
            System.out.println("4. Salir");
            System.out.println("Ingrese una opcion: ");
            opcion = entrada.nextInt();

            //evalua la opcion ingresada
            switch (opcion) {
                case 1:
                    pila.ingresar();
                    break;
                case 2:
                    pila.eliminar();
                    break;
                case 3:
                    pila.mostrar();
                    break;
                case 4:
                    break;
                default:
                    System.out.println("\nIngrese una opcion valida");
            }
        } while (opcion != 4);
    }
}

