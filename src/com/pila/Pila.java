package com.pila;

import java.util.Scanner;

public class Pila {
    //declara una variable de tipo Scanner para la entrada de los datos
    Scanner teclado = new Scanner(System.in);
    //se declara un arreglo numerico
    int[] pilaNumerica = new int[5];
    //se declara el tope que nos ayuda como indice del arreglo
    int tope = -1;

    //metodo para ingresar elementos a la pila
    public void ingresar() {
        //si el tope es mayor o igual a 4
        if(tope>=pilaNumerica.length-1) {
            System.out.println("La pila esta llena.");
        }
        else {
            //al tope le suma 1 y se agrega el elemento a la pila
            tope+=1;
            System.out.println("Ingrese el dato: ");
            pilaNumerica[tope] = teclado.nextInt();
        }
    }
    //metodo para eliminar los elementos de la pila
    public void eliminar() {
        if(tope == -1) {
            //si el tope es igual a -1 no hay items en la pila
            System.out.println("La pila esta vacia");
        }
        else {
            System.out.println("Se ha eliminado un elemento de la pila.");
            pilaNumerica[tope] = 0;
            tope-= 1;
        }
    }
    //Metodo para mostrar los elementos de la pila
    public void mostrar() {
        //desde que sea igual a 4 y mayor o igual a 0
        for(int i = pilaNumerica.length-1; i >= 0; i--) {
            System.out.println("Datos de la pila: " + pilaNumerica[i]);
        }
    }
}
